from flask import Flask,render_template, url_for, request, redirect, session, flash
from form import LoginForm, RegisterForm, FluxForm
from models import User, drop_tables, create_tables, Flux
import sqlite3 as lite
import re, click
import feedparser
from flask_login import LoginManager, current_user, login_user, logout_user, login_required, UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.config['SECRET_KEY'] = "lkkajdghdadkglajkgah" # a secret key for your app
login_manager = LoginManager(app)
login_manager.login_view = 'login'



#################### REGISTER AND LOGIN  ######################
@login_manager.user_loader
def load_user(user_id):
    return User.get(User.id == user_id)
 
 
@app.route('/')
@login_required
def index():
    return render_template('index.html') 

@app.route('/signin', methods=['GET', 'POST'])
def signin():
    form = RegisterForm()
    if form.validate_on_submit():
        user = User(
            username=form.username.data,
            password=generate_password_hash(form.password.data),
            email=form.email.data
        )
        user.save()
        flash('Account create.')
        return redirect(url_for('index'))
    
    return render_template('signin.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.get(User.username == form.username.data)
        if check_password_hash(user.password, form.password.data):
            login_user(user)
            flash('Happy to see you!')
            return redirect(url_for('index'))
        flash('Password ou usernam invalid')
    return render_template('login.html', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/addFlux', methods=['GET', 'POST'])
@login_required
def addFlux():
    form=FluxForm()
    user = User.get(User.username == current_user.username)

    if request.method == 'POST' and len(form.flux.data) > 3:
        
        try:
            flux = Flux.get(Flux.name == form.flux.data)
            user.fluxs.add(flux)
        except Exception as ex:
            flux = Flux(name=form.flux.data, user=user)
            flux.save()
            user.fluxs.add(flux)

        flash('The flox %s is save' % flux.name)
        form.flux.data = ""

    return render_template('UserAddFlux.html',form=form) 

 
@app.route('/VoirFlux', methods=['GET'])
@login_required
def testFlux():
    #https://starcraft2.judgehype.com/nouvelles.xml
    user = User.get(User.username == current_user.username)

    userFlux = []
    for flux in user.fluxs.order_by(Flux.name):
        userFlux.append(feedparser.parse(flux.name))

    print(len(userFlux))
    
    return render_template('UserFlux.html', userFlux=userFlux)


#################### Commandes DB ######################

@app.cli.command()
def initdb():
    """Create database"""
    create_tables()
    click.echo('Initialized the database')

@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()
    click.echo('Dropped tables from database')