from peewee import *
import click
from flask_login import UserMixin

database = SqliteDatabase("FluxBDD.sqlite3")

######################################################################@
#Classes

class BaseModel(Model):

    class Meta:
        database = database

class User(BaseModel, UserMixin):
    username = CharField()
    password = CharField()
    email = CharField()   

class Flux(BaseModel):
    name = CharField()
    users = ManyToManyField(User, backref='fluxs')

UserFlux = Flux.users.get_through_model()

######################################################################@
#Functions

def create_tables():
    with database:
        #database.create_tables([User, ])
        database.create_tables([Flux, User, UserFlux])


def drop_tables():
    with database:
        #database.drop_tables([User, ])
        database.drop_tables([Flux, User, UserFlux])