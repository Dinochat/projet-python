from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, PasswordField, SubmitField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length, EqualTo

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[
                       DataRequired(), Length(min=3, max=20)])
    password = PasswordField('Password', validators=[
                       DataRequired(), Length(min=3, max=20)])


class RegisterForm(FlaskForm):
    username = StringField('Username', validators=[
                       DataRequired(), Length(min=3, max=20)])
    password = PasswordField('Password', validators=[
                       DataRequired(), Length(min=3, max=20)])
    verify_password = PasswordField('verify', validators=[EqualTo('password')])
    email = StringField('Email')

class FluxForm(FlaskForm):
   flux = StringField('Name', validators=[
                       DataRequired(), Length(min=3, max=200)])
